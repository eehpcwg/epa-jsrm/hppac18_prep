ALL_FILES = $(wildcard *.tex) $(wildcard *.bib) $(wildcard */*.png) 

MAIN = main 
TARGET = $(MAIN).pdf 

.PHONY: all tidy clean 

all: clean $(TARGET) 

$(TARGET): $(ALL_FILES)
	pdflatex $(MAIN)
	bibtex $(MAIN)
	pdflatex $(MAIN)
	pdflatex $(MAIN)
	make tidy 

tidy:
	rm -f *.aux */*/*.aux */*/*/*.aux
	rm -f *.bbl */*/*.bbl */*/*/*.bbl
	rm -f *.log */*/*.log */*/*/*.log
	rm -f *.blg */*/*.blg */*/*/*.blg
	rm -f *.out */*/*.out */*/*/*.out
	rm -f *.lof */*/*.lof */*/*/*.lof
	rm -f *.lot */*/*.lot */*/*/*.lot
	rm -f *.toc */*/*.toc */*/*/*.toc 

clean:	tidy
	rm -f $(MAIN).pdf

